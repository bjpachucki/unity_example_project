﻿using System;

public interface IPlayerInput
{
    event Action<int> HotKeyPressed;
    event Action MoveModeTogglePressed;

    float Vertical { get; }
    float MouseX { get; }
    float Horizontal { get; }
    
    void Tick();
}